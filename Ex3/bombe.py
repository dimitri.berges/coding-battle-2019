import math
MAXSPEED = 100
coordsBomb = input().split(' ')
coordsBomb[0] = float(coordsBomb[0])
coordsBomb[1] = float(coordsBomb[1])

intsNT = input().split(' ')
N = int(intsNT[0])
T = int(intsNT[1])

listS = [[0 for y in range(T)] for i in range(N)]
nomsSuspect = [0 for i in range(N)]
goodSuspects = [False for i in range(N)]

for i in range(N):
    nomsSuspect[i] = input()
    for j in range(T):
        listS[i][j] = input().split(' ')
        listS[i][j][0] = float(listS[i][j][0])
        listS[i][j][1] = float(listS[i][j][1])

def calcDist(p1,p2):  
    dist = math.sqrt((p2[0] - p1[0])**2 + (p2[1] - p1[1])**2)  
    return dist

def calcDists(p1, p2, p3):
    return calcDist(p1, p3) + calcDist(p2, p3)
for i in range(N):
    for j in range(0, T - 1):
        if (calcDists(listS[i][j], listS[i][j + 1], coordsBomb) <= MAXSPEED):
            goodSuspects[i] = True
            break
goodSuspectString = ''
for i in range(N):
    if goodSuspects[i]:
        goodSuspectString += nomsSuspect[i] + " "
print(goodSuspectString)